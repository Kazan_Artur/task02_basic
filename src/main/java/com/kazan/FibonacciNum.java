package com.kazan;

/**
 * Generate a fibonacci sequence and make some procedure.
 *
 * @author Artur Kazan
 */

final class FibonacciNum {
    /**
     * Empty constructor.
     */
    private FibonacciNum() {
    }

    /**
     * <p>Method generate a fibonacci sequence</p>
     * Method generate a fibonacci sequence in interval that you write.
     *
     * @param start  is the first number of sequence
     * @param second is the second number of sequence
     * @param size   is quantity of numbers in sequence
     * @return array [] int
     */

    static int[] getFibonacciSequence(final int start,
                                      final int second,
                                      final int size) {
        int[] fibonacci = new int[size];
        fibonacci[0] = start;
        fibonacci[1] = second;
        for (int i = 2; i < fibonacci.length; i++) {
            fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
        }
        return fibonacci;
    }

    /**
     * <p>Method calculate a sum .</p>
     *
     * @param arr to calculate
     * @return int
     */

    static int getSum(final int[] arr) {
        int sum = 0;
        for (int i = 0; i < arr.length; i++) {
            sum = sum + arr[i];
        }
        return sum;
    }

    /**
     * <p>Method calculate a percentage of odd number.</p>
     *
     * @param arr to calculate
     * @return double
     */
    public static double calcPercentageOdd(final int[] arr) {
        int numOfOdd = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0) {
                numOfOdd++;
            }
        }
        int l = arr.length;
        double percentage = (double) numOfOdd / l;
        final int oneHundred = 100;
        return percentage * oneHundred;
    }
}


