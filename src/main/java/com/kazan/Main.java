package com.kazan;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;


/**
 * The application main class.
 * <h1>Java Basic Lesson application </h1>
 *
 * @author Artur Kazan
 * @version 1.0 08 Nov 2019
 */


public final class Main {
    /**
     * Empty constructor.
     */
    private Main() { }
    /**
     * The entry point of application.
     *
     *
     * @param args the input arguments
     * @throws IOException if readline dont work
     */
    public static void main(final String[] args) throws IOException {

        int oddNumSum = 0;
        int evenNumSum = 0;
        BufferedReader scanner = new BufferedReader(new InputStreamReader(
                System.in,
                StandardCharsets.UTF_8));
        System.out.println("Please, write a start number for interval:");
        int start = Integer.parseInt(scanner.readLine());
        System.out.println("Please, write  an end number for interval:");
        int end = Integer.parseInt(scanner.readLine());

        /**
         * Let's check numbers that you write
         */
        if (start > end) {
            System.out.println("You write incorrect numbers,"
                    + " the start number can't be higher than end one");
        }
        /**
         * * Now we will show all odd numbers
         *and find their sum
         */
        for (int i = start; i <= end; i++) {
            if (checkNumberForOdd(i)) {
                System.out.println(i + " is odd");
                oddNumSum += i;
            }
        }
        System.out.println("Sum of all "
                + " odd numbers from your interval is: " + oddNumSum);
        /**
         * * Now we find  all even numbers
         * and find their sum
         */
        for (int i = start; i <= end; i++) {
            if (i % 2 != 0) {
                System.out.println(i + " is even");
                evenNumSum += i;
            }
        }
        System.out.println("Sum of all "
                + " even numbers from your interval is: " + evenNumSum);


        /**
         * Get and printout a fibonacci sequence.
         * Start of sequence is the biggest odd number from interval.
         * Second number in sequence is the biggest even number.
         */
        if (start < end) {
            System.out.println("Please, enter a size of fibonacci row:");
            int size = Integer.parseInt(scanner.readLine());
            int oddBig = 0;
            int evenBig = 0;
            for (int i = start; i <= end; i++) {
                if (!checkNumberForOdd(i)) {
                    if (oddBig < i) {
                        oddBig = i;
                    } else if (evenBig < i) {
                        evenBig = i;
                    }
                }
            }
            int[] fibonacciArr =
                    FibonacciNum.getFibonacciSequence(oddBig, evenBig, size);
            for (int i = 0; i < fibonacciArr.length; i++) {
                System.out.println("Fibonacci number_" + i + "= "
                        + fibonacciArr[i]);
            }


            /**
             * Calc and printout a sum of the fibonacci sequence.
             */
            System.out.println("Sum of the fibonacci sequence= "
                    + FibonacciNum.getSum(fibonacciArr));

            /**
             * Calc and printout a percentage of odd and even number in array.
             */
            double perctageOfOdd = FibonacciNum.calcPercentageOdd(fibonacciArr);
            System.out.println("Percentage of odd numbers= " + perctageOfOdd);
            final int oneHund = 100;
            double perctageOfEven = oneHund - perctageOfOdd;
            System.out.println("Percentage of even numbers= " + perctageOfEven);
        }
    }

    /**
     * Check our numbers for odd.
     *
     * @param number from interval
     * @return number
     */
    private static boolean checkNumberForOdd(final int number) {
        return number % 2 == 0;

    }

}





